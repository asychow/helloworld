﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace HelloWorldTest
{
    [TestClass]
    public class MyClassTest
    {
        [TestMethod]
        public void TestMethod1()
        {
            Int64 candidate = 0;
            Int64 result = HelloWorld.MyClass.NegateAnInt(candidate);
            Assert.AreEqual<Int64>(0, result);
        }

        [TestMethod]
        public void TestMethod2()
        {
            Int64 candidate = 100;
            Int64 result = HelloWorld.MyClass.NegateAnInt(candidate);
            Assert.AreEqual<Int64>(-100, result);
        }

        [TestMethod]
        public void TestMethod3()
        {
            Int64 candidate = -55;
            Int64 result = HelloWorld.MyClass.NegateAnInt(candidate);
            Assert.AreEqual<Int64>(55, result);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TestMethod4()
        {
            Int64 candidate = Int64.MinValue;
            Int64 result = HelloWorld.MyClass.NegateAnInt(candidate);
        }
    }
}
