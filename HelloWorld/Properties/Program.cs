﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloWorld
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            Console.Read();
        }
    }

    public class MyClass
    {
        // Change the sign of an integer
        public static Int64 NegateAnInt(Int64 x)
        {
            // -Int64.MinValue exceeds Int64.MaxValue
            //  Int64 Range: [−2^63 to 2^63 − 1] 
            if (x == Int64.MinValue)
                throw (new ArgumentException());

            return -x;
        }
    }
}
